# Profile System Example
# ----------------------
#
#   this script profiles system interface operational status and saves to file
#
#   bash$ TESTBED=pyats0/testbeds/bed1.yaml DEVICE=uut PTSFILE=`pwd`/my_pts robot pyats0/scripts/profile_system.robot

#


*** Settings ***
Library        ats.robot.pyATSRobot
Library        genie.libs.robot.GenieRobot
Library        unicon.robot.UniconRobot

*** Variables ***
${timeout}     900

*** Test Cases ***
Initialize
    # initialize testbed by loading it through Genie
    use genie testbed "%{TESTBED}"

Connect to devices
    connect to device "%{DEVICE}"
    set unicon execute timeout to "${timeout}" seconds

Save interface profile as file
	Profile the system for "interface" on devices "%{DEVICE}" as "%{PTSFILE}"