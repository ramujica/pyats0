# Load Configuration Example
# --------------------------
#
#   this script loads configuration from file and apply to device
#
#   bash$ TESTBED=pyats0/testbeds/bed1.yaml DEVICE=uut CONFIG=pyats0/configs/CSR1.cfg robot pyats0/scripts/load_config.robot
#   bash$ TESTBED=pyats0/testbeds/bed1.yaml DEVICE=helper CONFIG=pyats0/configs/CSR2.cfg robot pyats0/scripts/load_config.robot





*** Settings ***
Library        OperatingSystem
Library        ats.robot.pyATSRobot
Library        unicon.robot.UniconRobot


*** Variables ***
${timeout}     120

*** Test Cases ***
Load testbed
    # load testbed file
    use testbed "%{TESTBED}"

Connect to devices
    connect to device "%{DEVICE}"
    set unicon configure timeout to "${timeout}" seconds

Load Configuration
    ${configuration}=  get file  %{CONFIG}
	configure "${configuration}" on device "%{DEVICE}"